#include "thirdparty/easylogging/easylogging++.h"

INITIALIZE_EASYLOGGINGPP

//#include "gdbstub/Server.h"
#include "core/msp430/Device.h"

#define log el::Loggers::getLogger("main")

std::vector<uint8_t> read_file(const char* path) {
    uint8_t raw[0x10000];
    FILE* fwfile = fopen(path, "rb");
    fread(raw, 0x10000, 1, fwfile);
    fclose(fwfile);

    auto data = std::vector<uint8_t>(0x10000);
    data.assign(raw, raw + 0x10000);

    return data;
}

int main(int argc, char* argv[]) {
    log->info("MSP430 Virtual machine started");
    auto target = std::make_shared<msp430::Device>(0x8000, 0x4000);
    // auto server = gdb::Server(27026, target);
    // server.doListen();

    if (argc != 1) {
        log->error("Wrong number of args...");
        return -1;
    }

    auto fw = read_file(argv[1]);
    target->memory->blockWrite(0, fw);

    target->init();
    for (int count = 0; count < 100; count++) {
        target->execute();
    }

    return 0;
}
