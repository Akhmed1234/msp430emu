//
// Created by Alexei Gladkikh on 24/09/16.
//

#ifndef MSP430EMU_MOV_H
#define MSP430EMU_MOV_H

#include "../../base/interfaces/AInstruction.h"

namespace dummy {
    class mov : public vmc::AInstruction {
    public:
        mov(const vmc::PAOperand vop1, const vmc::PAOperand vop2, const int size) :
                AInstruction("mov", vop1, vop2, size) { }

        bool execute(vmc::PDevice dev) {
            auto value = op2->value(dev);
            op1->value(dev, value);
            return true;
        }
    };
}

#endif //MSP430EMU_MOV_H
