//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_DUMMY_REGISTER_H
#define MSP430EMU_DUMMY_REGISTER_H

#include "../../base/operands/ARegister.h"
#include "../../base/exceptions/exceptions.h"

namespace dummy {
    class Register : public vmc::ARegister {
        uint32_t value(vmc::PDevice dev) override {
            return 0;
        }

        void value(vmc::PDevice dev, uint32_t data) override {
            return;
        }

        std::string stringify() override {
            return std::string("R0");
        }
    };
}

#endif //MSP430EMU_REGISTER_H
