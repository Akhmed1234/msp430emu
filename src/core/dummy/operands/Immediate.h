//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_DUMMY_IMMEDIATE_H
#define MSP430EMU_DUMMY_IMMEDIATE_H

#include "../../base/interfaces/AOperand.h"

namespace dummy {
    class Immediate : public vmc::AOperand {

    };
}

#endif //MSP430EMU_IMMEDIATE_H
