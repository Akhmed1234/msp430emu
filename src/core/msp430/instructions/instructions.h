//
// Created by Ignaty Deltsov on 26.09.2016.
//

#ifndef MSP430EMU_MSP430_INSTRUCTIONS_H_H
#define MSP430EMU_MSP430_INSTRUCTIONS_H_H

#include "mov.h"
#include "add.h"
#include "addc.h"
#include "subc.h"
#include "sub.h"
#include "cmp.h"
//#include "dadd.h"
#include "bit.h"
#include "bic.h"
#include "bis.h"
#include "xor.h"
#include "and.h"
// #include "rrc.h"
// #include "swpb.h"
// #include "rra.h"
// #include "sxt.h"
#include "and.h"
#include "push.h"
#include "call.h"
// #include "reti.h"
#include "jnz.h"
#include "jz.h"
#include "jnc.h"
#include "jc.h"
#include "jn.h"
#include "jge.h"
#include "jl.h"
#include "jmp.h"

#endif //MSP430EMU_MSP430_INSTRUCTIONS_H_H
