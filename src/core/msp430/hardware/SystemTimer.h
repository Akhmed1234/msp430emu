//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MSP430_SYSTEMTIMER_H
#define MSP430EMU_MSP430_SYSTEMTIMER_H

#include <cstdint>
#include "../../base/interfaces/ATimer.h"

namespace msp430 {
    class SystemTimer : public vmc::ATimer {
    private:
        vmc::PDevice _dev;
    public:
        void dev(vmc::PDevice dev) {
            _dev = dev;
        }

        void update(uint64_t ticks) override {
            return;
        }
    };
}

#endif //MSP430EMU_MSP430_SYSTEMTIMER_H
