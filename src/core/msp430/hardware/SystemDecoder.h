//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MSP430_SYSTEMDECODER_H
#define MSP430EMU_MSP430_SYSTEMDECODER_H

#include <vector>
#include <utility>

#include "../../base/interfaces/ADecoder.h"
#include "../../base/interfaces/AOperand.h"
#include "../../base/exceptions/exceptions.h"
#include "../instructions/instructions.h"
#include "../operands/Register.h"
#include "../operands/Displacement.h"
#include "../operands/Memory.h"
#include "../operands/Void.h"
#include "../operands/Immediate.h"

namespace msp430 {
    class SystemDecoder : public vmc::ADecoder {
    private:
        vmc::PDevice _dev;
    public:
        void dev(vmc::PDevice dev) {
            _dev = dev;
        }

        std::pair<vmc::PAOperand, vmc::PAOperand> parse_operand(std::vector<uint8_t> data, uint16_t opcode, int& cmdsize) {
            vmc::PAOperand op1 = nullptr;
            vmc::PAOperand op2 = nullptr;

            data.erase(data.begin(), data.begin() + 2);

            if ((opcode & 0xF000) >> 0x0C == 4) // Двухоперандная инсрукция
            {
                uint8_t src = (uint8_t) ((opcode & 0x0F00) >> 0x08);
                uint8_t dst = (uint8_t) (opcode & 0x000F);
                uint8_t ad = (uint8_t) ((opcode & 0x0080) >> 0x07);
                uint8_t as = (uint8_t) ((opcode & 0x0030) >> 0x04);
                uint8_t format = (uint8_t) ((opcode & 0x0040) >> 0x06); // Формат команды 0-слово, 1-байт
                printf("src = %02X, dst = %02X, as = %02X, ad = %02X, format = %02X\n", src, dst, as, ad, format);

                auto opsize = (format == 0) ? vmc::AOperand::WORD : vmc::AOperand::BYTE;

                // Источник
                if (src != 0 && src != 2 && src != 3) {
                    if (as == 0) // Регистровый
                    {
                        op1 = std::make_shared<Register>(src, opsize);
                    }
                    else if (as == 1) // Индексный - регистр - указатель на ячейку памяти + смещение
                    {
                        uint16_t offset = (data[1] << 0x8) + data[0];
                        data.erase(data.begin(), data.begin() + 2);
                        cmdsize += 2;
                        op1 = std::make_shared<Displacement>(src, offset, opsize);
                    }
                    else if (as == 2) // Косвенный регистровый - регистр - указатель, без смещения
                    {
                        op1 = std::make_shared<Displacement>(src, 0, opsize);
                    }
                    else // Косвенный регистровый с автоинкрементом - после операции увеличить адресс в регистре на 2
                    {
                        op1 = std::make_shared<Displacement>(src, 0, opsize, true); // ?????
                    }
                }
                else if (src == 0) {
                    if (as == 1) // Символьный (относительный) - регистр-указать-PC + смещение
                    {
                        uint16_t offset = (data[1] << 0x8) + data[0];
                        data.erase(data.begin(), data.begin() + 2);
                        cmdsize += 2;
                        op1 = std::make_shared<Displacement>(0, offset, opsize);
                    }
                    else if (as == 3)// Непосредственный - операнд - слово после текущей команды
                    {
                        uint16_t immediate = (data[1] << 0x8) + data[0];
                        data.erase(data.begin(), data.begin() + 2);
                        cmdsize += 2;
                        op1 = std::make_shared<Immediate>(immediate, opsize);
                    }
                }
                else if (src == 2) {
                    if (as == 1) // Абсолютный - адрес в памяти
                    {
                        uint16_t address = (data[1] << 0x8) + data[0];
                        data.erase(data.begin(), data.begin() + 2);
                        cmdsize += 2;
                        op1 = std::make_shared<Memory>(address, opsize);
                    }
                    else if (as == 2) // Константа 4
                    {
                        op1 = std::make_shared<Immediate>(4, opsize);
                    }
                    else if (as == 3) {
                        op1 = std::make_shared<Immediate>(8, opsize);
                    }
                }
                else {
                    if (as == 0) {
                        op1 = std::make_shared<Immediate>(0, opsize);
                    }
                    else if (as == 1) {
                        op1 = std::make_shared<Immediate>(1, opsize);
                    }
                    else if (as == 2) {
                        op1 = std::make_shared<Immediate>(2, opsize);
                    }
                    else if (as == 3) {
                        op1 = std::make_shared<Immediate>(0xFFFF, opsize);
                    }
                }

                // Приемник
                if (dst != 0 && dst != 2)
                {
                    if (!ad) // Регистровый тип адрессации
                    {
                        op2 = std::make_shared<Register>(dst, opsize);
                    }
                    else // Индексный - регистр указатель на ячейку памяти + смещение
                    {
                        uint16_t offset = (data[1] << 0x8) + data[0];
                        data.erase(data.begin(), data.begin() + 2);
                        cmdsize += 2;
                        op2 = std::make_shared<Displacement>(dst, offset, opsize);
                    }
                }
                else if (dst == 0) // Символьный (относительный) - регистр-указать-PC + смещение
                {
                    uint16_t offset = (data[1] << 0x8) + data[0];
                    data.erase(data.begin(), data.begin() + 2);
                    cmdsize += 2;
                    op2 = std::make_shared<Displacement>(0, offset, opsize);
                }
                else // Абсолютный - адресс в памяти
                {
                    uint16_t address = (data[1] << 0x8) + data[0];
                    data.erase(data.begin(), data.begin() + 2);
                    cmdsize += 2;
                    op2 = std::make_shared<Memory>(address, opsize);
                }
            }
            else if ((opcode & 0xE000)>>0x0D == 0) // Однооперандная инструкция
            {
                op2 = std::make_shared<Void>();
                uint8_t src = (uint8_t) (opcode & 0x000F);
                uint8_t as = (uint8_t) ((opcode & 0x0030) >> 0x04);
                uint8_t format = (uint8_t) ((opcode & 0x0040) >> 0x06);

                auto opsize = (format == 0) ? vmc::AOperand::WORD : vmc::AOperand::BYTE;

                if ((opcode & 0x0380)>>0x07 != 5)
                {
                    op1 = std::make_shared<Register>(src, opsize);
                }
                else if (src != 0 && src != 2 && src != 3)
                {
                    if (as == 0) // Регистровый
                    {
                        op1 = std::make_shared<Register>(src, opsize);
                    }
                    else if (as == 1) // Индексный - регистр - указатель на ячейку памяти + смещение
                    {
                        uint16_t offset = (data[1] << 0x8) + data[0];
                        data.erase(data.begin(), data.begin() + 2);
                        cmdsize += 2;
                        op1 = std::make_shared<Displacement>(src, offset, opsize);
                    }
                    else if (as == 2) // Косвенный регистровый - регистр - указатель, без смещения
                    {
                        op1 = std::make_shared<Displacement>(src, 0, opsize);
                    }
                    else // Косвенный регистровый с автоинкрементом - после операции увеличить адресс в регистре на 2
                    {
                        op1 = std::make_shared<Displacement>(src, 0, opsize, true); // ?????
                    }
                }
                else if (src == 0)
                {
                    if (as == 1) // Символьный (относительный) - регистр-указать-PC + смещение
                    {
                        uint16_t offset = (data[1] << 0x8) + data[0];
                        data.erase(data.begin(), data.begin() + 2);
                        cmdsize += 2;
                        op1 = std::make_shared<Displacement>(0, offset, opsize);
                    }
                    else if (as == 3)// Непосредственный - операнд - слово после текущей команды
                    {
                        uint16_t immediate = (data[1] << 0x8) + data[0];
                        data.erase(data.begin(), data.begin() + 2);
                        cmdsize += 2;
                        op1 = std::make_shared<Immediate>(immediate, opsize);
                    }
                }
                else if (src == 2 && as == 1)
                { // Абсолютный - адрес в памяти
                    uint16_t address = (data[1] << 0x8) + data[0];
                    data.erase(data.begin(), data.begin() + 2);
                    cmdsize += 2;
                    op1 = std::make_shared<Memory>(address, opsize);
                }
            }
            else if ((opcode & 0xE000)>>0x0D == 1) // Инструкция условного перехода
            {
                op2 = std::make_shared<Void>();
                auto opsize = vmc::AOperand::WORD;
                uint8_t format = 0;
                uint16_t offset = uint16_t(opcode & 0x03FF);
                op1 = std::make_shared<Memory>(offset, opsize);
            }
            return std::pair<vmc::PAOperand, vmc::PAOperand>(op1, op2);
        }

        vmc::PAInstruction decode(std::vector<uint8_t> data) override {
            vmc::PAInstruction instruction = nullptr;

            int cmdsize = 2;
            uint16_t opcode = (data[1]<<0x08) + data[0];
            printf("%02x %02x -> %04x \n", data[0], data[1], opcode);
            auto operands = parse_operand(data, opcode, cmdsize);
            vmc::PAOperand op1 = operands.first;
            vmc::PAOperand op2 = operands.second;

            if ((opcode & 0xE000)>>0x0D == 0) {

                if ((opcode & 0x0380)>>0x07 == 0) {
                    // instruction = std::make_shared<msp430::Rrc>(op1, op2, cmdsize);
                    instruction = nullptr;
                }
                else if ((opcode & 0x0380)>>0x07 == 1) {
                    // instruction = std::make_shared<msp430::Swpb>(op1, op2, cmdsize);
                    instruction = nullptr;
                }
                else if ((opcode & 0x0380)>>0x07 == 2) {
                    // instruction = std::make_shared<msp430::Rra>(op1, op2, cmdsize);
                    instruction = nullptr;
                }
                else if ((opcode & 0x0380)>>0x07 == 3) {
                    // instruction = std::make_shared<msp430::Sxt>(op1, op2, cmdsize);
                    instruction = nullptr;
                }
                else if ((opcode & 0x0380)>>0x07 == 4) {
                    instruction = std::make_shared<msp430::Push>(op1, cmdsize);
                }
                else if ((opcode & 0x0380)>>0x07 == 5) {
                    instruction = std::make_shared<msp430::Call>(op1, cmdsize);
                }
                else if ((opcode & 0x0380)>>0x07 == 6) {
                    // instruction = std::make_shared<msp430::Reti>(op1, op2, cmdsize);
                    instruction = nullptr;
                }
            }
            else if ((opcode & 0xE000)>>0x0D == 1){

                if ((opcode & 0x1C00)>>0x0A == 0) {
                    instruction = std::make_shared<msp430::Jnz>(op1);
                }
                else if ((opcode & 0x1C00)>>0x0A == 1) {
                    instruction = std::make_shared<msp430::Jz>(op1);
                }
                else if ((opcode & 0x1C00)>>0x0A == 2) {
                    instruction = std::make_shared<msp430::Jnc>(op1);
                }
                else if ((opcode & 0x1C00)>>0x0A == 3) {
                    instruction = std::make_shared<msp430::Jc>(op1);
                }
                else if ((opcode & 0x1C00)>>0x0A == 4) {
                    instruction = std::make_shared<msp430::Jn>(op1);
                }
                else if ((opcode & 0x1C00)>>0x0A == 5) {
                    instruction = std::make_shared<msp430::Jge>(op1);
                }
                else if ((opcode & 0x1C00)>>0x0A == 6) {
                    instruction = std::make_shared<msp430::Jl>(op1);
                }
                else if ((opcode & 0x1C00)>>0x0A ==7) {
                    instruction = std::make_shared<msp430::Jmp>(op1);
                }
            }
            else {

                if ((opcode & 0xF000)>>0x0C == 4) {
                    instruction = std::make_shared<msp430::Mov>(op1, op2, cmdsize);
                }
                else if ((opcode & 0xF000)>>0x0C == 5) {
                    instruction = std::make_shared<msp430::Add>(op1, op2, cmdsize);
                }
                else if ((opcode & 0xF000)>>0x0C == 6) {
                    // instruction = std::make_shared<msp430::Addc>(op1, op2, cmdsize);
                    instruction = nullptr;
                }
                else if ((opcode & 0xF000)>>0x0C == 7) {
                    // instruction = std::make_shared<msp430::Subc>(op1, op2, cmdsize);
                    instruction = nullptr;
                }
                else if ((opcode & 0xF000)>>0x0C == 8) {
                    instruction = std::make_shared<msp430::Sub>(op1, op2, cmdsize);
                }
                else if ((opcode & 0xF000)>>0x0C == 9) {
                    instruction = std::make_shared<msp430::Cmp>(op1, op2, cmdsize);
                }
                else if ((opcode & 0xF000)>>0x0C == 10) {
                    // instruction = std::make_shared<msp430::Dadd>(op1, op2, cmdsize);
                    instruction = nullptr;
                }
                else if ((opcode & 0xF000)>>0x0C == 11) {
                    instruction = std::make_shared<msp430::Bit>(op1, op2, cmdsize);
                }
                else if ((opcode & 0xF000)>>0x0C == 12) {
                    instruction = std::make_shared<msp430::Bic>(op1, op2, cmdsize);
                }
                else if ((opcode & 0xF000)>>0x0C == 13) {
                    instruction = std::make_shared<msp430::Bis>(op1, op2, cmdsize);
                }
                else if ((opcode & 0xF000)>>0x0C == 14) {
                    instruction = std::make_shared<msp430::Xor>(op1, op2, cmdsize);
                }
                else if ((opcode & 0xF000)>>0x0C == 15) {
                    instruction = std::make_shared<msp430::And>(op1, op2, cmdsize);
                }
            }
            return instruction;
        }
    };
}

#endif //MSP430EMU_MSP430_SYSTEMDECODER_H
