//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_MSP430_REGISTER_H
#define MSP430EMU_MSP430_REGISTER_H


#include "../../base/operands/ARegister.h"
#include "../../base/exceptions/exceptions.h"

namespace msp430 {
    class Register : public vmc::ARegister {
    public:
        Register(int vid, Size vsize) : vmc::ARegister(size), id(vid) {
            size = vsize;
        }

        const int id;

        // TODO: Add 16-bit mask on value
        uint32_t value(vmc::PDevice dev) const override {
            if (size == BYTE)
                return dev->cpu->reg(id) & 0xFF;
            else if (size == WORD)
                return dev->cpu->reg(id) & 0xFFFF;
            else
                throw new exc::UnsupportOperationException();
        }

        // TODO: Add 16-bit mask on value
        void value(vmc::PDevice dev, uint32_t data) const override {
            if (size == BYTE)
                dev->cpu->reg(id, data & 0xFF);
            else if (size == WORD)
                dev->cpu->reg(id, data & 0xFFFF);
            else
                throw new exc::UnsupportOperationException();
        }

        std::string stringify() const override {
            return utils::sformat("R%d", id);
        }

        static const std::shared_ptr<Register> PC;
        static const std::shared_ptr<Register> SP;
        static const std::shared_ptr<Register> SR;
    };
}

#endif //MSP430EMU_MSP430_REGISTER_H
