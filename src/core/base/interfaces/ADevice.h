//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_IDEVICE_H
#define MSP430EMU_IDEVICE_H

#include <list>
#include "ACPU.h"
#include "IMMU.h"
#include "ATimer.h"
#include "ADecoder.h"
#include "AInstruction.h"
#include "AOperand.h"
#include "APeripheral.h"
#include "AMemory.h"
#include "../../../thirdparty/easylogging/easylogging++.h"

#define log el::Loggers::getLogger("main")

namespace vmc {
    class ADevice : public std::enable_shared_from_this<ADevice> {
    public:
        const PACPU cpu;
        const PMemory memory;
        const PIMMU mmu;
        const PATimer timer;
        const PDecoder decoder;
        const std::list<PAPeripheral> periphs;

        ADevice(
                PACPU vcpu = nullptr,
                PMemory vmemory = nullptr,
                PIMMU vmmu = nullptr,
                PATimer vtimer = nullptr,
                PDecoder vdecoder = nullptr
        ) : cpu(vcpu), memory(vmemory), mmu(vmmu), timer(vtimer), decoder(vdecoder) { }

        std::shared_ptr<ADevice> getptr() {
            return shared_from_this();
        }

        virtual bool init() = 0;

        bool execute() {
            auto data = memory->blockRead(cpu->pc, 6);
            auto insn = decoder->decode(data);
            printf("size = %d, pc = %04x\n", insn->size, cpu->pc);
            insn->ea = cpu->pc;
            log->info(insn->stringify());
            cpu->pc += insn->size;
            auto pdev = this->getptr();
            if (!insn->execute(pdev)) {
                log->error("Exception occurred at %s", insn->stringify());
                return false;
            }
            timer->update(insn->ticks);
            return true;
        }
    };

    typedef std::shared_ptr<ADevice> PDevice;
}

#undef log

#endif //MSP430EMU_IDEVICE_H
