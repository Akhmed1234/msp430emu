//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_PERIPHERAL_H
#define MSP430EMU_PERIPHERAL_H

#include <cstdint>
#include <memory>
#include <list>
#include "AMemoryRegister.h"

namespace vmc {
    class APeripheral {
    public:
        const std::list<PAMemoryRegister> registers;
        const std::string name;
        const uint32_t vectorAdressIR;
        const uint32_t priority;
        bool interuptEnableBit;
        bool interuptPendingFlag;
    };

    typedef std::shared_ptr<APeripheral> PAPeripheral;
}

#endif //MSP430EMU_PERIPHERAL_H
