//
// Created by Alexei Gladkikh on 18/09/16.
//

#ifndef MSP430EMU_AREGISTER_H
#define MSP430EMU_AREGISTER_H

#include "../interfaces/AOperand.h"

namespace vmc {
    class ARegister : public AOperand {
    public:
        ARegister(Size vsize) : AOperand(REGISTER, vsize) { };
    };
}

#endif //MSP430EMU_AREGISTER_H
